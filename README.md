# Chorded Keymap

Chorded Keymap is a plugin for [Interception Tools](https://gitlab.com/interception/linux/tools) that temporarily remaps the keyboard when a set of keys (a "chord") is pressed simultaneously.

## Status

This project is in the prototype stage.
Currently, when the keys `s` and `d` are pressed simultaneously, it performs the following mapping:

| Key | Mapped key |
| --- | ---------- |
| `h` | `<Left arrow>`   |
| `j` | `<Down arrow>`   |
| `k` | `<Up arrow>`   |
| `l` | `<Right arrow>`   |

## Usage

To use Chorded Keymap, simply pipe the output of `intercept` into `chorded_keymap.py` before piping it back to `uinput` as discussed in the Intercpetion Tools documentation:

    intercept -g $DEVNODE | python chorded_keymap.py | uinput -d $DEVNODE

`chorded_keymap.py` requires Python 3.
It has been tested the most with Python 3.6, though any version greater than 2.x should work. 

## Future work

Ideally, the following features would be added:

* More complex chord handling: right now the chord gets eaten if pressed and released. It would be nice to let the keys pass through if pressed and released quickly, so that if you press `d` and then `s` before releasing `d` when typing for example `towards` the `ds` on the end does not get eaten.
* Allow additional keys to act as modifier keys: for example, while `s` and `d` are pressed simultaneously, `a` could like `<Shift>` and `f` could act like `<Control>`.
* Configuration: allow the chord keys, the mapping, and chord timeout to be specified in a configuration file.
* Multiple maps: currenlty, only `s` and `d` are supported. One might want to have `d` and `f` set a different keymap.
* Bigger chords: one might want to press three or four keys simultaneously for different maps.
* Code refactor: Chorded Keymap is written very simply right now because the above features are not implemented. To implement them, Chorded Keymap will need to be rewritten in a more modular and extensible fashion.
* Performance testing: for fast implementation, Chorded Keymap was written in Python. It seems to run fine like this, but it may be necessary to rewrite in another language for speed.
* Testing: there are no unit tests right now, which is a little scary given that all keyboard events are filtered through this program.
* Map keys to other actions: a mapped key could fork another process or generate a DBus event for example. Some of this functionality could be tricky to do write since `intercept` runs as root and the kinds of things you would want to map to would probably be associated with a different user and a particular graphical session.

## Background

This project is inspired by the [Simultaneous Vi Mode](https://github.com/tekezo/Karabiner/blob/05ca98733f3e3501e0679814c3795d1cb57e177f/src/core/server/Resources/include/checkbox/simultaneouskeypresses_vi_mode.xml#L4-L10) for [Karabiner](https://pqrs.org/osx/karabiner/), a keyboard mapping extension for macOS.
Because of architecture changes introduced in the Sierra version of macOS, Simultaneous Vi Mode does not work in Karabiner on the latest versions of macOS (see [here](https://github.com/tekezo/Karabiner-Elements/issues/153)).
Another related macOS project is [Jason Rudolph's keyboard project](https://github.com/jasonrudolph/keyboard) which used Karabiner's Simultaneous Vi Mode in the past and now uses an alternative implementation.

Similar functionality has not been easily accessible on Linux.
The closest there has been is the [Plover stenography project](http://www.openstenoproject.org/).
While it may have been possible to work from Plover to implement Chorded Keymap, this approach was not followed because Plover relies on the X display server to capture keyboard input and most Linux desktop environments are moving towards implementing the Wayland protocol in which applications and device input are more strongly isolated.
Interception Tools operates at the libevdev level, just above the kernel before keyboard events are processed by higher level abstractions like libinput, xkbcommon, or the window manager of a graphical desktop.
Working at this low level allows Interception Tools to work under many different systems, but it means that the state of the keyboard must tracked within Chorded Keymap.

## References

In addition to the [Interception Tools](https://gitlab.com/interception/linux/tools) documentation, the following references are useful:

* [How Input Works -- Keyboard Input](https://blog.martin-graesslin.com/blog/2016/12/how-input-works-keyboard-input/)
* [Understanding evdev](https://who-t.blogspot.de/2016/09/understanding-evdev.html)
* [evemu](https://www.freedesktop.org/wiki/Evemu/) -- another project that can record and generate libevdev events, though geared towards debugging rather than keyboard remapping.

## License

Like Interception Tools, Chorded Keymap uses the GPLv3 license.

<a href="https://gitlab.com/wsha/chorded_keymap/blob/master/LICENSE.md">
    <img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GPLv3">
</a>

Copyright © 2017 wsha
